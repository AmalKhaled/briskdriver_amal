//
//  StartDrivingViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 2/4/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit
import GoogleMaps
import SocketIO

let kMapStyle = "[" +
    "  {" +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"E4DAFA\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"elementType\": \"labels.icon\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"visibility\": \"off\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#616161\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"elementType\": \"labels.text.stroke\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#f5f5f5\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"administrative.land_parcel\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#bdbdbd\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"poi\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#D6C4FF\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"poi\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#757575\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"poi.park\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#D6C4FF\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"poi.park\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#9e9e9e\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#FCFCFE\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.arterial\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#757575\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.highway\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#EFE8FF\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.highway\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#616161\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"road.local\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#9e9e9e\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"transit.line\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#FCFCFE\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"ttransit.station\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#D6C4FF\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"water\"," +
    "    \"elementType\": \"geometry\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#D6C4FF\"" +
    "      }" +
    "    ]" +
    "  }," +
    "  {" +
    "    \"featureType\": \"water\"," +
    "    \"elementType\": \"labels.text.fill\"," +
    "    \"stylers\": [" +
    "      {" +
    "        \"color\": \"#6B46C1\"" +
    "      }" +
    "    ]" +
    "  }," +
"]"


class StartDrivingViewController: UIViewController {
    @IBOutlet weak var mapViewer: UIView!
    
    @IBOutlet weak var onlineSwitch: UISwitch!
    
    @IBOutlet weak var babySeatSwitch: UISwitch!
    
    var locationManager = CLLocationManager()
    let regionRadius: CLLocationDistance = 1000
    
    let mapView = GMSMapView()
    var lng = 0.0
    var lat = 0.0
    var hasBaby = true
    var isActive = true
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        setUpView()
        setupMap()
        login_socket()
        getTrip()

    }
    
    @IBAction func onlineSwitchAction(_ sender: Any) {
        isActive = !isActive
        if !isActive{
            SocketConnections.socketConnections.driverStatusSocket(isActive: isActive)
            SocketConnections.socketConnections.closeConnection()
        }
        else {
            SocketConnections.socketConnections.driverStatusSocket(isActive: isActive)
            
        }
        
        
    }
    @IBAction func babySeatAction(_ sender: Any) {
        hasBaby = !hasBaby
        login_socket()
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension StartDrivingViewController{
    func setupMap(){
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
//        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
          mapViewer = mapView

        mapView.isMyLocationEnabled = true
        do {
            // Set the map style by passing a valid JSON string.
            mapView.mapStyle = try GMSMapStyle(jsonString: kMapStyle)
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        mapView.delegate = self
        //        bMapView.gesture / = true
    }
}
extension StartDrivingViewController: CLLocationManagerDelegate , GMSMapViewDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //        print("updated")
        //        if (IsReturned){
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!,
                                              longitude: (location?.coordinate.longitude)!,
                                              zoom: 15.0)
        
        //
        lng = (location?.coordinate.longitude)!
        lat = (location?.coordinate.latitude)!
        
        //            IsReturned = false
        mapView.camera = camera
        mapView.animate(to: camera)
        
        driverUpdateLocationSocket()
        //        }
    }
    
    
    func login_socket(){
        
        SocketConnections.socketConnections.login_socket(lat: lat, lng: lng, hasBaby: hasBaby)
        
        
    }
    
    func driverUpdateLocationSocket(){
        SocketConnections.socketConnections.driverUpdateLocationSocket(lat: lat, lng: lng)
    }
    func getTrip(){
        SocketConnections.socketConnections.getTripSocket()
    }
    
}
