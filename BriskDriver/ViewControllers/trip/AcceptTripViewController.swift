//
//  AcceptTripViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 2/5/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit
import GoogleMaps

class AcceptTripViewController: UIViewController {

    @IBOutlet weak var toDetailslbl: UILabel!
    @IBOutlet weak var toLocationLbl: UILabel!
    @IBOutlet weak var fromDetailsLbl: UILabel!
    @IBOutlet weak var fromLocationLbl: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var acceptBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func rejectAction(_ sender: Any) {
    }
    
    @IBAction func acceptAction(_ sender: Any) {
    }
}
