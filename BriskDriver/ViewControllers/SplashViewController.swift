//
//  ViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/27/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            if AppDelegate.defaults.string(forKey: "token") != nil{
                AppDelegate.currentDriver.token = AppDelegate.defaults.string(forKey: "token")!
                
                
                Constants.HEADER["Authorization"] = "Bearer \(AppDelegate.currentDriver.token)"
                self.getUserData()
                //
                
            }
            else {
                //go to login"
                self.performSegue(withIdentifier: "login", sender: nil)
            }
            
            
        }
    }
    func getUserData(){
    
            UserController.userController.getProfile(completion: {
            check , msg in
            if check == 0 {
                if let vc = UIStoryboard(name: "Trip", bundle: nil).instantiateViewController(withIdentifier: "start_driving") as? StartDrivingViewController
                {
                    
                    self.present(vc, animated: true, completion: nil)
                }
                
                
            }
            else if check == 1{
                self.performSegue(withIdentifier: "login", sender: self)
                
            }
            else {
                self.noLoginInternetDialog()
            }
        }
        )
    }
}



