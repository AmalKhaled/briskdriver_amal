//
//  ValidateSignupCodeViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 2/1/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit
import PinCodeTextField
//import SRCountdownTimer

class ValidateSignupCodeViewController: UIViewController {
    @IBOutlet var textFields: [PinCodeTextField]!
    @IBOutlet weak var pinCodeTF: PinCodeTextField!
    @IBOutlet weak var timerLabel: UIButton!
    
    @IBOutlet weak var pinCodeErrorLbl: UILabel!
    @IBOutlet weak var cantGetCodeBtn: UIButton!
    @IBOutlet weak var verifyBtn: UIButton!
    @IBOutlet weak var userEmailLbl: UILabel!
    
    var timer = Timer()
    
    var timeLeft: TimeInterval = 60
    var endTime: Date?
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setupCounter ()
        setData()
        pinCodeTF.delegate = self
        // Do any additional setup after loading the view.
        //        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    @IBAction func verifiyBtnAction(_ sender: Any) {
        self.performSegue(withIdentifier: "pending", sender: self)
        
        
        UserController.userController.valideSignUp(completion: {check, msg in
            
            
        }, code: self.pinCodeTF.text!)
        
        
    }
    
    @IBAction func canntGetCodebtnAction(_ sender: Any) {
        timerLabel.isEnabled = false
        UserController.userController.validationResendCode(completion: {
            check ,msg in
            
            if check == 0{
                
                self.view.makeToast(msg)
                self.timeLeft = 60
                self.setupCounter ()
            }
            else if check == 1{
                self.timerLabel.isEnabled = true
                
                self.view.makeToast(msg)
            }
            else {
                self.timerLabel.isEnabled = true
                
                self.noInternetDialog()
            }
            
        })
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension ValidateSignupCodeViewController{
    
    func setData(){
        userEmailLbl.text = AppDelegate.currentDriver.email
    }
    
    func stopTimer(){
        timer.invalidate()
        //                timer = nil
    }
    func setupCounter (){
        
        timerLabel.setTitle(timeLeft.time, for: .normal)
        
        endTime = Date().addingTimeInterval(timeLeft)
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        
    }
    @objc func updateTime() {
        if timeLeft > 0 {
            timerLabel.isEnabled = false
            timeLeft = endTime?.timeIntervalSinceNow ?? 0
            timerLabel.setTitle( timeLeft.time, for: .normal)
        } else {
            timerLabel.isEnabled = true
            timerLabel.setTitle( "resend code", for: .normal)
            timer.invalidate()
            
        }
    }
}
extension ValidateSignupCodeViewController: PinCodeTextFieldDelegate{
    func textFieldValueChanged(_ textField: PinCodeTextField) {
        
        if pinCodeTF.text!.count == 4{
            verifyBtn.isEnabled = true
            verifyBtn.alpha = 1
        }
        else {
            verifyBtn.isEnabled = false
            verifyBtn.alpha = 0.5
        }
        
    }
}
