//
//  LoginViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/30/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit
import CountryList
import Toast_Swift

class LoginViewController: UIViewController {
    
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var countryCodeTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var passwordErrorLbl: UILabel!
    @IBOutlet weak var phoneErrorLbl: UILabel!
    var countryList = CountryList()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: nil)
        countryList.delegate = self
        
    }
    
    @IBAction func loginBtnAction(_ sender: Any) {
        loginButton.isEnabled = false
        loginButton.alpha = 0.5
        UserController.userController.login(completion: {check, msg in
            
            if check == 0{
                if !AppDelegate.currentDriver.isActive{
                    self.performSegue(withIdentifier: "valid_number", sender: self)
                }
                else if !AppDelegate.currentDriver.isVerified {
                    
                }
                else {
                    if let vc = UIStoryboard(name: "Trip", bundle: nil).instantiateViewController(withIdentifier: "start_driving") as? StartDrivingViewController
                         {
                             
                            self.present(vc, animated: true, completion: nil)
                         }
                }
                
                
            }
            else if check == 1{
                self.view.makeToast(msg)
//                if !AppDelegate.currentDriver.isActive{
//                                   self.performSegue(withIdentifier: "valid_number", sender: self)
//                               }
//                "Data": {
//                      "is_active": 0,
//                      "mobile_number": "1234567890",
//                      "email": "a@a.aa"
//                  }
            }
            else {
                self.loginButton.isEnabled = true
                self.loginButton.alpha = 1
                self.noInternetDialog()
            }
            
        }, phoneNumber: phoneNumberTF.text! ,phoneCode: countryCodeTF.text!, password: passwordTF.text!)
        
        
    }
    @IBAction func becoumACaptainAction(_ sender: Any) {
        self.performSegue(withIdentifier: "sign_up", sender: self)
    }
    @IBAction func forgetpasswordBtnAction(_ sender: Any) {
        self.performSegue(withIdentifier: "forget_pass", sender: self)
    }
    
    @objc private func textDidChange(_ notification: Notification) {
        enableButton()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension LoginViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == countryCodeTF{
            phoneNumberTF.becomeFirstResponder()
        }
        else   if textField == phoneNumberTF{
            passwordTF.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == phoneNumberTF){
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == countryCodeTF {
            let navController = UINavigationController(rootViewController: countryList)
            if #available(iOS 13.0, *) {
                countryList.isModalInPresentation = true
            } else {
                // Fallback on earlier versions
                
            }
            self.present(navController, animated: true, completion: nil)
            return false
            
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let (valid , text) = ValidTextField(textField: textField)
        
        if textField == phoneNumberTF || textField == countryCodeTF {
            phoneErrorLbl.isHidden = valid
            phoneErrorLbl.text = text
        }
        else if textField == passwordTF {
            passwordErrorLbl.isHidden = valid
            passwordErrorLbl.text = text
        }
        if valid {
            textField.layer.borderWidth = 0
            
        }
        else{
            textField.layer.borderWidth = 1
            textField.layer.borderColor = UIColor.red.cgColor
        }
        
    }
}
extension LoginViewController{
    
    
    
    func enableButton(){
        var formIsValid = true
        
        for textField in textFields {
            // Validate Text Field
            let (valid,_) = ValidTextField(textField: textField)
            
            guard valid else {
                formIsValid = false
                break
            }
        }
        loginButton.isEnabled = formIsValid
        if (formIsValid){
            loginButton.alpha = 1
        }
        else {
            loginButton.alpha = 0.5
            
        }
    }
    func ValidTextField(textField : UITextField)->(Bool, String?) {
        if textField == phoneNumberTF{
            if textField.text!.count == 0{
                return (false ,NSLocalizedString("enter your phone number",comment:"") )
                
            }
            else {
                if (countryCodeTF.text!.count != 0){
                    if checkValidPhonNumber(Phone: countryCodeTF.text!+phoneNumberTF.text!){
                        return (true ,nil )
                        
                    }
                    else {
                        return (false ,NSLocalizedString("enter valid phone number".lowercased(),comment:"") )
                    }
                }
                else{
                    return (true ,nil )
                }
                
                
            }
        }
        else if textField == countryCodeTF{
            
            if countryCodeTF.text!.count == 0{
                return (false ,NSLocalizedString("choose country phone code".lowercased(),comment:"") )
            }
            else {
                if (phoneNumberTF.text!.count != 0){
                    if checkValidPhonNumber(Phone: countryCodeTF.text!+phoneNumberTF.text!){
                        return (true ,nil )
                        
                    }
                    else {
                        return (false ,NSLocalizedString("enter valid phone number".lowercased(),comment:"") )                               }
                }
                else{
                    return (true ,nil )
                }
                
            }
        }
        else if textField == passwordTF{
            if textField.text!.count < 6{
                return (false ,NSLocalizedString("password should be greater than 6 digits",comment:"") )
                
            }
            else {
                return (true ,nil )
                
            }
        }
        return (true ,nil )
    }
    
    
}
extension LoginViewController : CountryListDelegate{
    func selectedCountry(country: Country) {
        countryCodeTF.text! = country.phoneExtension
        let (valid,messsage) =  ValidTextField(textField: countryCodeTF)
        
        
        phoneErrorLbl.isHidden = valid
        phoneErrorLbl.text = messsage
        if valid {
            countryCodeTF.layer.borderWidth = 0
            
        }
        else{
            countryCodeTF.layer.borderWidth = 1
            countryCodeTF.layer.borderColor = UIColor.red.cgColor
        }
        
    }
}
