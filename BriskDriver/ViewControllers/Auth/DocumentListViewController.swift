//
//  DocumentListViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/31/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit

class DocumentListViewController: UIViewController {
    
    let docHeaders = ["Profile Picture", "Emirates ID Front", "Emirate ID Back", "Driving Licnse Front","Driving Licnse Back", "Car Registration Front", "Car Registration Back", "PTA Card Front", "PTA Card Back"]
    var imagesDic = [Int: UIImage]()
    var currentIndex = 0
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    var driver = Driver()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setUpView()
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        
        nextBtn.isEnabled = false
        nextBtn.alpha = 0.1

        
        UserController.userController.signUp(completion: {check,msg in
            if check == 0{
                self.performSegue(withIdentifier: "sucees", sender: self)
            }
            else if check == 1{
                self.view.makeToast(msg)
                
            }
            else {
                self.nextBtn.isEnabled = true
                self.nextBtn.alpha = 1
                self.noInternetDialog()
            }
            
        }, driver: driver, Images: imagesDic)
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension DocumentListViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docHeaders.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DocumentListTableViewCell
        cell.setData(header: docHeaders[indexPath.row], at: indexPath.row)
        cell.Delegate = self
        return cell
    }
}



extension DocumentListViewController: DocumentListDelegate{
    func getCellIndex(cellIndex: Int) {
        
        var imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary;
        imagePicker.allowsEditing = true
        currentIndex = cellIndex
        self.present(imagePicker, animated: true, completion: nil)
        //
    }
    
    
}
extension DocumentListViewController{
    func enableButton() {
        if imagesDic.count == 9{
            nextBtn.isEnabled = true
            nextBtn.alpha = 1
        }
        else {
            nextBtn.isEnabled = false
            nextBtn.alpha = 0.5
        }
        
    }
}

extension DocumentListViewController : UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        
        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.editedImage)] as? UIImage {
            //            userImageView.image = image
            imagesDic[currentIndex] = image
            
            
            
        }
        else if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            imagesDic[currentIndex] = image
            
        } else{
            self.view.makeToast(NSLocalizedString("Something went wrong",comment:""))
        }
        let cell = tableView.cellForRow(at: [0,currentIndex]) as! DocumentListTableViewCell
        
        cell.DoneIcone.isHidden = false
        
        enableButton()
        
        dismiss(animated:true, completion: nil)
        
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
