//
//  SignUpViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/30/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit
import CountryList
import Toast_Swift

class SignUpViewController: UIViewController {
    
    @IBOutlet var textFields: [UITextField]!
    
    @IBOutlet weak var fNameTF: UITextField!
    @IBOutlet weak var lNameTF: UITextField!
    @IBOutlet weak var countryTF: UITextField!
    
    @IBOutlet weak var countryCodeTF: DesignableUITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    @IBOutlet weak var phoneErrorLbl: UILabel!
    @IBOutlet weak var emailErrorLbl: UILabel!
    @IBOutlet weak var passwordErrorLbl: UILabel!
    
    
    @IBOutlet weak var countryErrorLbl: UILabel!
    
    @IBOutlet weak var lNameErrorLbl: UILabel!
    @IBOutlet weak var fNameErrorLbl: UILabel!
    
    @IBOutlet weak var SignUpBtn: UIButton!
    var countryList = CountryList()
    var countryType = "number"
    var driver = Driver()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setUpView()
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: nil)
        countryList.delegate = self
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        SignUpBtn.isEnabled = false
        SignUpBtn.alpha = 0.5
        
        driver.fName = fNameTF.text!
        driver.lName = lNameTF.text!
        driver.country = countryTF.text!
        driver.countryCode = countryCodeTF.text!
        driver.phoneNumber = phoneNumberTF.text!
        driver.email = emailTF.text!
        driver.password = passwordTF.text!
        self.performSegue(withIdentifier: "doc_list", sender: self)
        self.SignUpBtn.isEnabled = true
        self.SignUpBtn.alpha = 1
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func textDidChange(_ notification: Notification) {
        enableButton()
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        
        if segue.identifier == "doc_list"{
            var dVC = segue.destination as! DocumentListViewController
            dVC.driver = self.driver
        }
     }
     
    
    
}
extension SignUpViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fNameTF{
            lNameTF.becomeFirstResponder()
        }
        else if textField == lNameTF{
            countryTF.becomeFirstResponder()
        }
        else if textField == countryTF{
            countryCodeTF.becomeFirstResponder()
        }
        else if textField == countryCodeTF{
            phoneNumberTF.becomeFirstResponder()
        }
        else if textField == phoneNumberTF{
            emailTF.becomeFirstResponder()
        }
        else if textField == emailTF{
            passwordTF.becomeFirstResponder()
        }
        else{
            textField.resignFirstResponder()
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == phoneNumberTF){
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == countryCodeTF {
            self.countryType = "number"
            let navController = UINavigationController(rootViewController: countryList)
            if #available(iOS 13.0, *) {
                countryList.isModalInPresentation = true
            } else {
                // Fallback on earlier versions
                
            }
            
            self.present(navController, animated: true, completion: nil)
            return false
            
        }
        else if textField == countryTF {
            self.countryType = "country"
            
            let navController = UINavigationController(rootViewController: countryList)
            if #available(iOS 13.0, *) {
                countryList.isModalInPresentation = true
            } else {
                // Fallback on earlier versions
                
            }
            self.present(navController, animated: true, completion: nil)
            return false
            
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let (valid , text) = ValidTextField(textField: textField)
        
        if textField == fNameTF{
            fNameErrorLbl.isHidden = valid
            fNameErrorLbl.text = text
        }
        else if textField == lNameTF{
            lNameErrorLbl.isHidden = valid
            lNameErrorLbl.text = text
        }
        else if textField == countryTF{
            countryErrorLbl.isHidden = valid
            countryErrorLbl.text = text
        }
        else if textField == emailTF{
            emailErrorLbl.isHidden = valid
            emailErrorLbl.text = text
        }
            
            
        else if textField == phoneNumberTF || textField == countryCodeTF {
            phoneErrorLbl.isHidden = valid
            phoneErrorLbl.text = text
        }
        else if textField == passwordTF {
            passwordErrorLbl.isHidden = valid
            passwordErrorLbl.text = text
        }
        if valid {
            textField.layer.borderWidth = 0
            
        }
        else{
            textField.layer.borderWidth = 1
            textField.layer.borderColor = UIColor.red.cgColor
        }
        
    }
}

extension SignUpViewController{
    
    
    
    func enableButton(){
        var formIsValid = true
        
        for textField in textFields {
            // Validate Text Field
            let (valid,_) = ValidTextField(textField: textField)
            
            guard valid else {
                formIsValid = false
                break
            }
        }
        SignUpBtn.isEnabled = formIsValid
        if (formIsValid){
            SignUpBtn.alpha = 1
        }
        else {
            SignUpBtn.alpha = 0.5
            
        }
    }
    func ValidTextField(textField : UITextField)->(Bool, String?) {
        
        
        
        
        if textField == phoneNumberTF{
            if textField.text!.count == 0{
                return (false ,NSLocalizedString("enter your phone number",comment:"") )
                
            }
            else {
                if (countryCodeTF.text!.count != 0){
                    if checkValidPhonNumber(Phone: countryCodeTF.text!+phoneNumberTF.text!){
                        return (true ,nil )
                        
                    }
                    else {
                        return (false ,NSLocalizedString("enter valid phone number".lowercased(),comment:"") )
                    }
                }
                else{
                    return (true ,nil )
                }
                
                
            }
        }
        else if textField == countryCodeTF{
            
            if countryCodeTF.text!.count == 0{
                return (false ,NSLocalizedString("choose country phone code".lowercased(),comment:"") )
            }
            else {
                if (phoneNumberTF.text!.count != 0){
                    if checkValidPhonNumber(Phone: countryCodeTF.text!+phoneNumberTF.text!){
                        return (true ,nil )
                        
                    }
                    else {
                        return (false ,NSLocalizedString("enter valid phone number".lowercased(),comment:"") )                               }
                }
                else{
                    return (true ,nil )
                }
                
            }
        }
            
        else if textField == fNameTF{
            if textField.text!.count == 0{
                return (false ,NSLocalizedString("enter your first name",comment:"") )
                
            }
            else {
                return (true ,nil )
                
            }
        }
        else if textField == lNameTF{
            if textField.text!.count == 0{
                return (false ,NSLocalizedString("enter your second name",comment:"") )
                
            }
            else {
                return (true ,nil )
                
            }
        }
        else if textField == countryTF{
            if textField.text!.count == 0{
                return (false ,NSLocalizedString("choose your country",comment:"") )
                
            }
            else {
                return (true ,nil )
                
            }
        }
        else if textField == emailTF{
            if textField.text!.count == 0 || !isValidEmail(testStr: textField.text!){
                return (false ,NSLocalizedString("enter right email",comment:"") )
                
            }
            else {
                return (true ,nil )
                
            }
        }
        else if textField == passwordTF{
            if textField.text!.count < 6{
                return (false ,NSLocalizedString("password should be greater than 6 digits",comment:"") )
                
            }
            else {
                return (true ,nil )
                
            }
        }
        return (true ,nil )
    }
    
    
}
extension SignUpViewController : CountryListDelegate{
    func selectedCountry(country: Country) {
        if countryType == "number"{
            countryCodeTF.text! = country.phoneExtension
            let (valid,messsage) =  ValidTextField(textField: countryCodeTF)
            
            
            phoneErrorLbl.isHidden = valid
            phoneErrorLbl.text = messsage
            if valid {
                countryCodeTF.layer.borderWidth = 0
                
            }
            else{
                countryCodeTF.layer.borderWidth = 1
                countryCodeTF.layer.borderColor = UIColor.red.cgColor
            }
        }
        else {
            countryTF.text! = country.name!
            let (valid,messsage) =  ValidTextField(textField: countryCodeTF)
            
            
            countryErrorLbl.isHidden = valid
            countryErrorLbl.text = messsage
            if valid {
                countryTF.layer.borderWidth = 0
                
            }
            else{
                countryTF.layer.borderWidth = 1
                countryTF.layer.borderColor = UIColor.red.cgColor
            }
        }
        
    }
}
