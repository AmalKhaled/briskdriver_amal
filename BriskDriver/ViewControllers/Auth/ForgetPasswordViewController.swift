//
//  ForgetPasswordViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 2/15/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit

class ForgetPasswordViewController: UIViewController {
    @IBOutlet var textFields: [UITextField]!
    
    @IBOutlet weak var emailErrorLbl: UILabel!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var emailAddressTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setUpView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func sendAction(_ sender: Any) {
        sendBtn.isEnabled = false
        sendBtn.alpha = 0.5
        
        UserController.userController.forgetPassword(completion: {
            check, msg in
            if check == 0{
                self.view.makeToast(msg)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self.dismiss(animated: true, completion: nil)
                    
                    
                })
            }
            else if check == 1{
                self.view.makeToast(msg)

            }
            else{
                self.noInternetDialog()
            }
            
            
        }, email: emailAddressTF.text!)
        
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc private func textDidChange(_ notification: Notification) {
        enableButton()
    }
}
extension ForgetPasswordViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return true
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let (valid , text) = ValidTextField(textField: textField)
        
        
        if textField == emailAddressTF{
            emailErrorLbl.isHidden = valid
            emailErrorLbl.text = text
        }
        
        
        
        if valid {
            textField.layer.borderWidth = 0
            
        }
        else{
            textField.layer.borderWidth = 1
            textField.layer.borderColor = UIColor.red.cgColor
        }
        
    }
}

extension ForgetPasswordViewController{
    
    
    
    func enableButton(){
        var formIsValid = true
        
        for textField in textFields {
            // Validate Text Field
            let (valid,_) = ValidTextField(textField: textField)
            
            guard valid else {
                formIsValid = false
                break
            }
        }
        sendBtn.isEnabled = formIsValid
        if (formIsValid){
            sendBtn.alpha = 1
        }
        else {
            sendBtn.alpha = 0.5
            
        }
    }
    func ValidTextField(textField : UITextField)->(Bool, String?) {
        
        
        
        
        if textField == emailAddressTF{
            if textField.text!.count == 0 || !isValidEmail(testStr: textField.text!){
                return (false ,NSLocalizedString("enter right email",comment:"") )
                
            }
            else {
                return (true ,nil )
                
            }
        }
        
        return (true ,nil )
    }
    
    
}
