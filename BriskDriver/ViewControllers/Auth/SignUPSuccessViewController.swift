//
//  SignUPSuccessViewController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 2/1/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit

class SignUPSuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func continuBtnAction(_ sender: Any) {
        self.performSegue(withIdentifier: "valid_number", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
