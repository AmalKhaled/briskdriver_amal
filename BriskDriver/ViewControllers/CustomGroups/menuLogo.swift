//
//  menuLogo.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 2/4/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit

class menuLogo: UIStackView {

     override init(frame: CGRect) {
          super.init(frame: frame)
          
          
      }
      
      required init(coder : NSCoder) {
          super.init(coder: coder)
          distribution = .fill
          alignment = .fill
          if let nibView = Bundle.main.loadNibNamed("menuLogo", owner: self, options: nil)?.first as? UIStackView {
                         addArrangedSubview(nibView)
                     }
      }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
