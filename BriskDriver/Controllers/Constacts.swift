//
//  Constacts.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/30/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import Foundation

class Constants{
    static var DOMAIN = "http://brisk-drive.com/brisk_driver/api/"
    static var LOGIN  = DOMAIN + "login"
    static var PROFILE  = DOMAIN + "profile"
    static var SIGNUP  = DOMAIN + "register"
    static var VALIDATE_SIGN_UP = DOMAIN + "verification"
    static var RESEND_CODE = DOMAIN + "resend/code/"
    static var FORGET_PASSWORD = DOMAIN + "password/forget/"

    
    static var SOCKET_DOMAIN = "http://brisk-drive.com:4444"
    static var LOGIN_SOCKET = "login_driver"
    static var DRIVER_STATUS_SOCKET = "online_offline"
    static var GET_TRIP_SOCKET = "new_trip"

    static var HEADER = [
        "Accept-Language": "1",
        "Content-Type": "application/json",
        "app-version": "1",
        "mobile-version": "1",
        "country-code": "971",
        "Accept": "application/json",
    ]
}
