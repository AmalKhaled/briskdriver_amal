//
//  UserController.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/30/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import Foundation
import UIKit

class UserController{
    static var userController = UserController()
    
    func login(completion: @escaping(Int,String)-> (), phoneNumber: String, phoneCode: String,password: String){
        let param = ["number":phoneNumber,
                     "phone_code": phoneCode,
                     "password":password,
                     "user_type": "driver",
                     "push_token": "aaaaaaaaaaa",
                     "device_type": "ios"
        ]
        
        APIConnection.apiConnection.postConnection(completion: {check, data, errors,msg in
            
            if check == 0{
                
                AppDelegate.currentDriver = Driver(with: data!["user"] as! NSDictionary as! [String:Any])
                AppDelegate.currentDriver.token = data!["token"] as! String
                AppDelegate.defaults.set(AppDelegate.currentDriver.token, forKey: "token")
                
                Constants.HEADER["Authorization"] = "Bearer \(AppDelegate.currentDriver.token)"
                completion(check, msg)
                
            }
            else if check == 1{
                
                if  data != nil{
                    AppDelegate.currentDriver = Driver(notActive: data as! [String:Any])
                    completion(0, msg)
                    
                }
                else {
                    completion(check, msg)
                    
                }
                
                
                
            }
            else {
                completion(check, msg)
                
            }
            
            
            
            
        }, link: Constants.LOGIN, param: param)
        
    }
    func getProfile(completion: @escaping(Int,String)-> ()){
        
        print(Constants.PROFILE)
        
        APIConnection.apiConnection.getConnection(completion: {check, data,_,msg in
            print(data )
            if check == 0{
                let token = AppDelegate.currentDriver.token
                AppDelegate.currentDriver = Driver(with: data as! [String:Any])
                AppDelegate.currentDriver.token = token
                
                completion(check, msg)
                
            }
            else if check == 1{
                
                
                completion(check, msg)
                
                
            }
            else {
                completion(check, msg)
                
            }
            
            
            
            
        }, link: Constants.PROFILE)
        
    }
    
    func signUp (completion: @escaping(Int,String)-> (), driver: Driver, Images: [Int:UIImage]){
        let param = ["frist_name": driver.fName,
                     "lat_name": driver.lName,
                     "country": driver.country,
                     "phone_code": driver.countryCode,
                     "mobile_number": driver.phoneNumber,
                     "email": driver.email,
                     "password": driver.password,
                     "user_type":"driver"]
        APIConnection.apiConnection.uploadFile(completion: {check,data,msg in
            
            if check == 0{
                AppDelegate.currentDriver = driver
                AppDelegate.currentDriver.register(with: data as! [String : Any])
                
            }
            
            completion(check, msg)
            
            
        }, link: Constants.SIGNUP, param: param, images: Images)
        
    }
    func valideSignUp (completion: @escaping(Int,String)-> (), code:String){
        let param = ["mobile_number": AppDelegate.currentDriver.phoneNumber,
                     "verification_code": code,
                     "user_type": "driver",
        ]
        APIConnection.apiConnection.postConnection(completion: {check,data,errors,msg  in
            
            completion(check, msg)
            
            
        }, link: Constants.VALIDATE_SIGN_UP, param: param)
        
    }
    func validationResendCode (completion: @escaping(Int,String)-> ()){
        
        let link = "\(Constants.RESEND_CODE)\(AppDelegate.currentDriver.email)/driver"
        
        
        print(link)
        
        APIConnection.apiConnection.getConnection(completion: {check,data,errors,msg  in
            
            
            completion(check, msg)
            
            
        }, link: link)
        
    }
    func forgetPassword (completion: @escaping(Int,String)-> (), email: String){
          
          let link = "\(Constants.FORGET_PASSWORD)\(email)/driver"
          
          
          print(link)
          
          APIConnection.apiConnection.getConnection(completion: {check,data,errors,msg  in
              
              
              completion(check, msg)
              
              
          }, link: link)
          
      }
}
