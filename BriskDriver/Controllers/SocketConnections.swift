//
//  SocketConnections.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 2/5/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import Foundation
import SocketIO
class SocketConnections{
    static var socketConnections = SocketConnections()
    let manager = SocketManager(socketURL: URL(string: Constants.SOCKET_DOMAIN)!, config: [.log(true), .compress])
    lazy var socket = manager.defaultSocket
    
    func establishConnection() {
        socket.connect()
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    func login_socket(lat: Double, lng: Double, hasBaby: Bool){
        
        let timeStamp = Date.currentTimeStamp
        let myJSON = [
            "driver_id": 105,
            "lat": lat,
            "lng": lng,
            "cat_id": 0,
            "sub_cat_id": 0,
            "car_baby": hasBaby,
            "update_at": timeStamp
            ] as [String : Any]
        
        print(myJSON)
        
        socket.on(Constants.LOGIN_SOCKET) {data, ack in
            
            self.socket.emit("data", myJSON)
            
            
        }
        socket.connect()

    }
    func driverStatusSocket(isActive: Bool) {
        
        let myJSON = [
            "driver_id": 105,
            "status": isActive,
            
            ] as [String : Any]
        socket.on(Constants.DRIVER_STATUS_SOCKET) {data, ack in
            
            self.socket.emit("data", myJSON)
            
        }
        socket.connect()
    }
    func driverUpdateLocationSocket(lat: Double , lng: Double){
        let timeStamp = Date.currentTimeStamp
        
        let myJSON = [
            "driver_id": 105,
            "lat": lat,
            "lng": lng,
            "update_at": timeStamp
            
            ] as [String : Any]
        
        print(myJSON)
        
        socket.on(Constants.DRIVER_STATUS_SOCKET) {data, ack in
            
            self.socket.emit("data", myJSON)
        }
        socket.connect()
    }
    
    func getTripSocket(){
        socket.on(Constants.GET_TRIP_SOCKET) {data, ack in
//            guard let data = data[0] as? NSDictionary else { return }
            
            print(data)
         
        }

        socket.connect()
    }
    
}
