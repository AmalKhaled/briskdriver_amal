//
//  APIConnection.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/30/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class APIConnection{
    static var apiConnection = APIConnection()
    
    func postConnection (completion: @escaping(Int,NSDictionary?,NSArray?,String)-> (), link : String, param: Parameters ){
        Alamofire.request(link, method: .post, parameters: param, encoding: JSONEncoding.default, headers: Constants.HEADER).responseJSON { response in
            
            if let JSON = response.result.value  as? NSDictionary{
                print(JSON)
                let msg = JSON["Message"] as! String
                
                if let check = JSON["Status"] as? Bool {
                    if check {
                        if let data = JSON["Data"] as? NSDictionary{
                            completion(0, data,nil,msg )
                        }
                        else{
                            completion(0, [:],nil,msg )
                            
                        }
                    }
                    else{
                        if let errors = response.result.value  as? NSArray{
                            completion(1,nil,errors,msg)
                            
                        }
                        else if let data = JSON["Data"] as? NSDictionary{
                            completion(1,data,nil,msg)

                        }
                        else {
                            completion(1, nil,nil,msg)
                        }
                    }
                }
                
            }
            else {
                completion(2, nil,nil,"noInternet")
            }
        }
    }
    func getConnection (completion: @escaping(Int,NSDictionary?,NSArray?,String)-> (), link : String ){
        print(Constants.HEADER)
        Alamofire.request(link, method: .get, encoding: JSONEncoding.default , headers: Constants.HEADER).responseJSON { response in
            
            if let JSON = response.result.value  as? NSDictionary{
                print(JSON)
                let msg = JSON["Message"] as! String
                
                if let check = JSON["Status"] as? Bool{
                    if check {
                        if let data = JSON["Data"] as? NSDictionary{
                            completion(0, data,nil,msg )
                        }
                        else{
                            completion(0, [:],nil,msg )
                            
                        }
                    }
                    else{
                        if let errors = response.result.value  as? NSArray{
                            completion(1,nil,errors,msg)
                            
                        }
                        else if let data = JSON["Data"] as? NSDictionary{
                            completion(1,data,nil,msg)

                        }
                        else {
                            completion(1, nil,nil,msg)
                        }
                    }
                }
                else {
                    completion(1,nil,nil,msg)

                }
                
            }
            else {
                completion(2, nil,nil,"noInternet")
            }
        }
    }
    
    func uploadFile (completion: @escaping(Int,NSDictionary?,String)-> (), link : String, param: [String: String], images: [Int:UIImage]){
        let imageParam = ["profile": images[0]!,
                          "nationality_id_front": images[1]!,
                          "nationality_id_back": images[2]!,
                          "driving_license_front": images[3]!,
                          "driving_license_back": images[4]!,
                          "car_registration_front": images[5]!,
                          "car_registration_back": images[6]!,
                          "pta_card_front": images[7]!,
                          "pta_card_back": images[8]!,
        ]
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                for (key, value) in imageParam {

                if let imageData = value.jpegData(compressionQuality: 0.1) {
                    multipartFormData.append(imageData, withName: key, fileName: "\(key).jpeg", mimeType: "\(key)/jpeg")
                    }
                }
                for (key, value) in param {
                    if let data = value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
                        multipartFormData.append(data, withName: key)
                        
                    }
                }
        },
            
            to: link,
            headers:Constants.HEADER,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        if let JSON = response.result.value  as? NSDictionary{
                            print(JSON)
                            let msg = JSON["Message"] as! String
                            
                            if let check = JSON["Status"] as? Bool{
                                if check {
                                    if let data = JSON["Data"] as? NSDictionary{
                                        completion(0, data, msg )
                                    }
                                    else{
                                        completion(0, [:],msg)
                                        
                                    }
                                    
                                }
                                else{
                                    completion(1, [:],msg)
                                    
                                }
                            }
                            
                        }
                    }
                    
                case .failure( _):
                    completion(2, [:],"noInternet")
                    
                }
        })
    }
    
    
    
    
    
}
