//
//  Driver.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/31/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import Foundation

class Driver{
    var fName = ""
    var lName = ""
    var email = ""
    var password = ""
    var countryCode = ""
    var phoneNumber = ""
    var country = ""
    var id = 0
    var profilePic = ""
    var isVerified = false
    var isActive = false
    var wallet = 0.0
    var catID = 0
    var subCatID = 0
    var token = ""
    
    init(){
        
    }
    init(with dictionary: [String:Any]?){
        guard let dictionary = dictionary else {return}
        id = dictionary["user_id"] as! Int
        fName = dictionary["first_name"] as! String
        lName = dictionary["last_name"] as! String
        email = dictionary["email"] as! String
        profilePic = dictionary["profile_path"] as! String
        phoneNumber = dictionary["mobile_number"] as! String
        isVerified = dictionary["is_verified"] as! Bool
        isActive = dictionary["is_active"] as! Bool
        wallet = Double(dictionary["user_wallet"] as! String) as! Double
        catID = dictionary["cat_id"] as! Int
        subCatID = dictionary["sub_cat_id"] as! Int
        
        
    }
    init(notActive dictionary: [String:Any]?){
        guard let dictionary = dictionary else {return}
        
        email = dictionary["email"] as! String
        
        phoneNumber = dictionary["mobile_number"] as! String
        isActive = dictionary["is_active"] as! Bool
        
        
        
    }
    func register (with dictionary: [String:Any]?){
        guard let dictionary = dictionary else {return}
        id = dictionary["user_id"] as! Int
        
    }
}
