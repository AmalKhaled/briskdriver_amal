//
//  DocumentListTableViewCell.swift
//  BriskDriver
//
//  Created by Amal Elgalant on 1/31/20.
//  Copyright © 2020 Amal Elgalant. All rights reserved.
//

import UIKit

public protocol DocumentListDelegate: class {
    func getCellIndex(cellIndex: Int)
}

class DocumentListTableViewCell: UITableViewCell {
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var ImageButton: UIButton!
    @IBOutlet weak var DoneIcone: UIImageView!
    
    open weak var Delegate: DocumentListDelegate?

    var index = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(header: String ,at index: Int){
        self.headerLbl.text = header
        self.DoneIcone.isHidden = true
        self.ImageButton.setTitle("     add \(header)", for: .normal)
        self.index = index
    }

    @IBAction func uploadImageAction(_ sender: Any) {
      
        
        
        Delegate?.getCellIndex(cellIndex: self.index)
    }
}
